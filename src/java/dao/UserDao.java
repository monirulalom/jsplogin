/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import model.User;

/**
 *
 * @author monirulalom
 */
public class UserDao {

    private Connection connection;

    public UserDao() {
        connectionclass con = new connectionclass();
        try {
            connection = con.getConnection();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public boolean login(User user) throws SQLException {
        boolean status = false;
        try {
            PreparedStatement preparedStatement = connection.prepareStatement("select * from user where email = ? and password = ? ");
            preparedStatement.setString(1, user.getEmail());
            preparedStatement.setString(2, user.getPassword());

            System.out.println(preparedStatement);
            ResultSet rs = preparedStatement.executeQuery();
            status = rs.next();

        } catch (SQLException e) {
            // handle exception
        }
        return status;
    }

    public int register(User user) throws SQLException {
        int res =0;
        try {
            PreparedStatement preparedStatement = connection.prepareStatement("insert into user(email,password) values(?,?) ");
            preparedStatement.setString(1, user.getEmail());
            preparedStatement.setString(2, user.getPassword());

            System.out.println(preparedStatement);
            res = preparedStatement.executeUpdate();

        } catch (SQLException e) {
            // handle exception
        }
        return res;
    }
}
